package irdm.aws;


import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;


public class BigramRelativeFrequency {

	public static class BigramCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

		private final Text bigram = new Text();
		private final static IntWritable one = new IntWritable(1);

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

			String line = value.toString();
			StringTokenizer itr = new StringTokenizer(line);
			
			String first = null;
			
			while (itr.hasMoreTokens()) {

				String second = itr.nextToken();
				if (first != null) {

					String bigramStr = first + " " + second;
					bigram.set(bigramStr);
					context.write(bigram, one);
					context.write(new Text(first + " " + "*"), one);
				} 
					first = second;						
			}
		}
	}
	
	public static class BigramCountReducer extends Reducer<Text, IntWritable, Text, DoubleWritable>{
		
		private final DoubleWritable totalCount = new DoubleWritable();
		private final DoubleWritable relativeCount = new DoubleWritable();
		private final Text flag = new Text("*");
		
		
		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException{
			
			String [] tokens = key.toString().split(" ");
			if(tokens[1].equals(flag.toString())){
				totalCount.set(0);
				totalCount.set(getTotalCount(values));
				
				context.write(key, totalCount);
			}
			else{
				int count = getTotalCount(values);
				relativeCount.set((double) count / totalCount.get());
				
				context.write(key, relativeCount);
			}
			
		}
	}
	
	private static int getTotalCount(Iterable<IntWritable> values){
		
		int count = 0;
		Iterator<IntWritable> itr = values.iterator();
        while(itr.hasNext()){
        	count += itr.next().get();
        }
        return count;
	}
	
	public static class bigramCombiner extends Reducer<Text,IntWritable,Text,IntWritable> {
	    private final IntWritable totalCount = new IntWritable();

	    @Override
	    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
	        int count = 0;
	        Iterator<IntWritable> itr = values.iterator();
	        while(itr.hasNext()){
	        	count += itr.next().get();
	        }
	        totalCount.set(count);
	        context.write(key, totalCount);
	    }
	}

	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
	    
	    //Starting point of execution
	    Configuration conf = new Configuration(); 
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs(); // get all args
	    if (otherArgs.length != 2) {
	      System.err.println("Usage: BigramCount <in> <out>");
	      System.exit(2);
	    }

	    // create a job with name "bigramcount"
	    Job job = new Job(conf, "bigramCount");
	    job.setJarByClass(BigramRelativeFrequency.class);
	    
	    job.setMapperClass(BigramCountMapper.class);
	    job.setReducerClass(BigramCountReducer.class);
	    job.setCombinerClass(bigramCombiner.class);
	     
	    // set output key type   
	    job.setOutputKeyClass(Text.class);
	    // set output value type
	    job.setOutputValueClass(IntWritable.class);
	    //set the HDFS path of the input data
	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    // set the HDFS path for the output
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));

	    //Wait till job completion
	    System.exit(job.waitForCompletion(true) ? 0 : 1);  
	        
	    }

}

