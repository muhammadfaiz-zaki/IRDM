package irdm.aws;

import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;


public class BigramCount {

	public static class BigramCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

		private final Text bigram = new Text();
		private final static IntWritable one = new IntWritable(1);

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

			String line = value.toString();
			StringTokenizer itr = new StringTokenizer(line);

			String first = null;

			while (itr.hasMoreTokens()) {

				String second = itr.nextToken();
				if (first != null) {

					String bigramStr = first + " " + second;
					bigram.set(bigramStr);
					context.write(bigram, one);
				} 
					first = second;				
			}
		}
	}
	
	public static class BigramCountReducer extends Reducer<Text, IntWritable, Text, IntWritable>{
		
		private final IntWritable sum = new IntWritable();
		
		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException{
			
			int totalSum = 0;
			Iterator<IntWritable> itr = values.iterator();
			
			while(itr.hasNext()){
				totalSum += itr.next().get();
			}
			
			sum.set(totalSum);
			context.write(key, sum);
		}
	}

	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
	    
	    //Starting point of execution
	    Configuration conf = new Configuration(); 
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs(); // get all args
	    if (otherArgs.length != 2) {
	      System.err.println("Usage: BigramCount <in> <out>");
	      System.exit(2);
	    }

	    // create a job with name "bigramcount"
	    Job job = new Job(conf, "bigramCount");
	    job.setJarByClass(BigramCount.class);
	    
	    job.setMapperClass(BigramCountMapper.class);
	    job.setReducerClass(BigramCountReducer.class);
	     
	    // set output key type   
	    job.setOutputKeyClass(Text.class);
	    // set output value type
	    job.setOutputValueClass(IntWritable.class);
	    //set the HDFS path of the input data
	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    // set the HDFS path for the output
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));

	    //Wait till job completion
	    System.exit(job.waitForCompletion(true) ? 0 : 1);  
	        
	    }

}
