
package irdm.aws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MapFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import irdm.aws.writables.*;

public class BuildInvertedIndex {

	public static class InvertedIndexMapper extends Mapper<LongWritable, Text, Text, PairOfInts> {

		private final Text word = new Text();

		public void map(LongWritable docno, Text value, Context context) throws IOException, InterruptedException {

			String line = value.toString();
			String[] tokens = line.split(" ");

			ArrayList<String> counted = new ArrayList<String>();

			for (int i = 0; i < tokens.length; i++) {
				if (tokens[i] == null || tokens[i].length() == 0) {
					continue;
				}

				int count = 0;

				String token = tokens[i];

				if (counted.contains(token))
					continue;

				for (int j = 0; j < tokens.length; j++) {

					if (tokens[j].equals(token)) {
						count++;
					}
				}
				counted.add(tokens[i]);
				word.set(tokens[i]);

				context.write(word, new PairOfInts((int) docno.get(), count));
			}
		}
	}

	public static class InvertedIndexReducer
			extends Reducer<Text, PairOfInts, Text, PairOfWritables<IntWritable, ArrayListWritable<PairOfInts>>> {

		private final static IntWritable docfreq = new IntWritable();

		public void reduce(Text key, Iterable<PairOfInts> values, Context context)
				throws IOException, InterruptedException {

			Iterator<PairOfInts> iter = values.iterator();
			ArrayListWritable<PairOfInts> postings = new ArrayListWritable<PairOfInts>();

			int df = 0;
			while (iter.hasNext()) {
				postings.add(iter.next().clone());
				df++;
			}

			Collections.sort(postings);

			docfreq.set(df);
			context.write(key, new PairOfWritables<IntWritable, ArrayListWritable<PairOfInts>>(docfreq, postings));

		}

	}

	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {

		// Starting point of execution
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

		if (otherArgs.length != 2) {
			System.err.println("Usage: BuildInvertedIndex <in> <out>");
			System.exit(2);
		}

		// create a job with name "bigramcount"
		Job job = new Job(conf, "Inverted Index");
		job.setJarByClass(BuildInvertedIndex.class);

		job.setMapperClass(InvertedIndexMapper.class);
		job.setReducerClass(InvertedIndexReducer.class);
		
		job.setMapOutputKeyClass(Text.class);
	    job.setMapOutputValueClass(PairOfInts.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(PairOfWritables.class);
	    job.setOutputFormatClass(MapFileOutputFormat.class);
		// set output key type
//		job.setOutputKeyClass(Text.class);
//		// set output value type
//		job.setOutputValueClass(PairOfWritables.class);
		// set the HDFS path of the input data
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		// set the HDFS path for the output
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));

		// Wait till job completion
		System.exit(job.waitForCompletion(true) ? 0 : 1);

	}

}
