
package irdm.aws;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;
import java.io.FileWriter;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;

import irdm.aws.writables.*;

public class AnalyzeBigramRelativeFrequency {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("usage: [input-path]");
			System.exit(-1);
		}

		System.out.println("input path: " + args[0]);

//		 List<PairOfWritables<PairOfStrings, FloatWritable>> pairs =
//		 SequenceFileUtils.readDirectory(new Path(args[0]));
		List<PairOfWritables<Text, DoubleWritable>> pairs;
		try {
			pairs = readDirectory(new Path(args[0]));

			List<PairOfWritables<Text, DoubleWritable>> list1 = new ArrayList<PairOfWritables<Text, DoubleWritable>>();
			double prob1 = 0,prob2 = 0,prob3 = 0;
			
			for (PairOfWritables<Text, DoubleWritable> p : pairs) {
				Text bigram = p.getLeftElement();
				String pairOfText = bigram.toString();
				
				String[] tokens = pairOfText.split(" ");
				
				
				if (tokens[0].equals("romeo")) {
					list1.add(p);
				}
				if (tokens[0].equals("romeo") && tokens[1].equals("is")){
					prob1 = p.getRightElement().get();
			
				}
				if (tokens[0].equals("is") && tokens[1].equals("the")){
					prob2 = p.getRightElement().get();
				}
				if (tokens[0].equals("the") && tokens[1].equals("king")){
					prob3 = p.getRightElement().get();
				}
				
			}
			
			Collections.sort(list1,
					new Comparator<PairOfWritables<Text, DoubleWritable>>() {
						public int compare(PairOfWritables<Text, DoubleWritable> e1,
								PairOfWritables<Text, DoubleWritable> e2) {
							if (e1.getRightElement().compareTo(e2.getRightElement()) == 0) {
								return e1.getLeftElement().compareTo(e2.getLeftElement());
							}

							return e2.getRightElement().compareTo(e1.getRightElement());
						}
					});
			
			
			FileWriter writer = new FileWriter(System.getProperty("user.dir") + "/src/irdm/aws/data/question2.txt");
			int i = 0;
			for (PairOfWritables<Text, DoubleWritable> p : list1) {
				Text bigram = p.getLeftElement();
				writer.write(bigram + "\t" + p.getRightElement()+"\n");
				i++;

				if (i > 5) {
					break;
				}
			}
			
			writer.write("probability of sentence romeo is the king: \n" );
			writer.write(String.format("%.4f", prob1*prob2*prob3));
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Reads in the bigram relative frequency count file
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	private static List<PairOfWritables<Text, DoubleWritable>> readDirectory(Path path)
			throws IOException {

		File dir = new File(path.toString());
		ArrayList<PairOfWritables<Text, DoubleWritable>> relativeFrequencies = new ArrayList<PairOfWritables<Text, DoubleWritable>>();
		for (File child : dir.listFiles()) {
			if (".".equals(child.getName()) || "..".equals(child.getName())) {
				continue; // Ignore the self and parent aliases.
			}
			FileInputStream bigramFile = null;

			bigramFile = new FileInputStream(child.toString());

			// Read in the file
			DataInputStream resultsStream = new DataInputStream(bigramFile);
			BufferedReader results = new BufferedReader(new InputStreamReader(resultsStream));

			StringTokenizer rToken;
			String rLine;
			String firstWord;
			String secondWord;
			String frequency;

			// iterate through every line in the file
			while ((rLine = results.readLine()) != null) {
				rToken = new StringTokenizer(rLine);
				// extract the meaningful information
				firstWord = rToken.nextToken();
				//remove leading ( and trailing ,
				//firstWord = firstWord.substring(1, firstWord.length() - 1);
				secondWord = rToken.nextToken();
				//remove trailing )
				//secondWord = secondWord.substring(0, secondWord.length() - 1);
				frequency = rToken.nextToken();
				try{
				relativeFrequencies.add(new PairOfWritables<Text, DoubleWritable>(
						new Text(firstWord + " " + secondWord), new DoubleWritable(Double.parseDouble(frequency))));
				}
				catch(NumberFormatException e){
					System.out.println(e);
				}
			}
			if (bigramFile != null)
				bigramFile.close();
		}

		return relativeFrequencies;

	}
}
