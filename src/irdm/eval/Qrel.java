package irdm.eval;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Qrel {
	private String document_id;
	private int topic_no;
	private int subtopic_no;
	private int judgment;

	/*
	 * Initialize the Qrel object with query, document and its relevance grade.
	 */
	public Qrel(String topic, String doc_id, String rel) {

		Pattern pattern = Pattern.compile("\\s");
		Matcher matcher = pattern.matcher(doc_id);
		boolean found = matcher.find();

		if (found) {
			String[] splitID = doc_id.split(" ");

			String toRemove = "/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/Dataset/clueweb12//";
			String removePost = ".txt";
			String temp = splitID[1].replace(toRemove, "");
			String docID = temp.replace(removePost, "");

			this.topic_no = Integer.parseInt(topic);
			this.document_id = docID;
			this.judgment = Integer.parseInt(rel);
		} else {
			this.topic_no = Integer.parseInt(topic);
			this.document_id = doc_id;
			this.judgment = Integer.parseInt(rel);
		}
	}

	/*
	 * Initialize the Qrel object with query, subtopic, document and its
	 * relevance grade with respect to query subtopic.
	 */
	public Qrel(String topic, String subtopic, String doc_id, String rel) {
		// TODO Auto-generated constructor stub
		this.topic_no = Integer.parseInt(topic);
		this.subtopic_no = Integer.parseInt(subtopic);
		this.document_id = doc_id;
		this.judgment = Integer.parseInt(rel);

	}

	public String toString() {
		//return document_id + " " + topic_no + " " + judgment;
		return topic_no + " " + subtopic_no + " " + document_id + " " + judgment;
	}

	public String getId() {
		return document_id;
	}

	public int getTopicNo() {
		return topic_no;
	}

	public int getJudgment() {
		return judgment;
	}
	
	public int getSubTopic() {
		return subtopic_no;
	}

}
