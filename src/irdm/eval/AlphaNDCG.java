package irdm.eval;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import irdm.utils.Utils;
import irdm.utils.Utils.QREL;

public class AlphaNDCG {

	private double alpha;
	private HashMap<Integer, LinkedHashMap<String, LinkedHashMap<Integer, Integer>>> qrel_dict = new HashMap<Integer, LinkedHashMap<String, LinkedHashMap<Integer, Integer>>>();
				

	private AlphaNDCG(double alpha) {
		
		this.alpha = alpha;
		
	}

	HashMap<Integer, Set<Integer>> loadJudgment(ArrayList<Qrel> qrel_list) {
		
		HashMap<Integer, Set<Integer>> topic = new HashMap<Integer, Set<Integer>>();
		LinkedHashMap<Integer, Integer> query_dd = new LinkedHashMap<Integer, Integer>();
		LinkedHashMap<String, LinkedHashMap<Integer, Integer>> doc_dd = new LinkedHashMap<String, LinkedHashMap<Integer, Integer>>();
		
		for (Qrel q : qrel_list) {
			
			if(!qrel_dict.containsKey(q.getTopicNo())){
				 doc_dd = new LinkedHashMap<String, LinkedHashMap<Integer, Integer>>();
			}
			if(!doc_dd.containsKey(q.getId())){
				//doc_dd = new LinkedHashMap<String, LinkedHashMap<Integer, Integer>>();
				query_dd = new LinkedHashMap<Integer, Integer>();
				query_dd.put(q.getSubTopic(), q.getJudgment());
				doc_dd.put(q.getId(), query_dd);
				
			}
			else{
				doc_dd.get(q.getId()).put(q.getSubTopic(), q.getJudgment());
			}
			qrel_dict.put(q.getTopicNo(), doc_dd);
			
			if (qrel_dict.get(q.getTopicNo()).get(q.getId()).get(q.getSubTopic()) > 0
					&& topic.containsKey(q.getTopicNo())) {
				topic.get(q.getTopicNo()).add(q.getSubTopic());
			} else if(qrel_dict.get(q.getTopicNo()).get(q.getId()).get(q.getSubTopic()) > 0){
				Set<Integer> intent = new HashSet<Integer>();
				intent.add(q.getSubTopic());
				topic.put(q.getTopicNo(), intent);
			}
			
		}
	
		return topic;
	}

	double idcg(Set<Integer> topic, int query_id, int depth) {

		HashMap<Integer, Double> topic_gain = new HashMap<Integer, Double>();
		Set<String> doc_ids = qrel_dict.get(query_id).keySet();
		ArrayList<String> doc_ids_list = new ArrayList<String>();
		doc_ids_list.addAll(doc_ids);
		double idcg = 0.0;

		depth = Math.min(doc_ids.size(), depth);

		int i = 0;
		while (i < depth) {
			ArrayList<Double> gains = new ArrayList<Double>();
			for (String id : doc_ids_list) {
				gains.add(i_gain(id, topic, query_id, topic_gain));
			}
			double max_gain = Collections.max(gains);
			int max_gain_index = gains.indexOf(max_gain);
			String max_docid = doc_ids_list.get(max_gain_index);

			for (Integer intent : topic) {
				if (qrel_dict.get(query_id).get(max_docid).get(intent) > 0) {
					if (!topic_gain.containsKey(intent)) {
						topic_gain.put(intent, 1.0);
						double tg = topic_gain.get(intent);
						tg *= 1.0 - this.alpha;
						topic_gain.put(intent, tg);
					} else {
						double tg = topic_gain.get(intent);
						tg *= 1.0 - this.alpha;
						topic_gain.put(intent, tg);
					}
				}
			}
			doc_ids_list.remove(max_gain_index);
			double tempidcg = max_gain /  discount(depth);
			if(Double.isNaN(tempidcg) || Double.isInfinite(tempidcg))
				tempidcg = 0.0;
			idcg += tempidcg;
			i++;
		}
		return idcg;
	}

	double dcg(ArrayList<String> doc_ids, Set<Integer> topic, int query_id, int depth) {

		HashMap<Integer, Double> topic_gain = new HashMap<Integer, Double>();

		double dcg = 0.0;
		depth = Math.min(doc_ids.size(), depth);
		
		for (int i = 0; i < depth; i++) {
			
			if(qrel_dict.get(query_id).containsKey(doc_ids.get(i))){
			double dg = gain(doc_ids.get(i), topic, query_id, topic_gain);
			double tempdcg = dg/discount(i);
			if(Double.isNaN(tempdcg) || Double.isInfinite(tempdcg))
				tempdcg = 0.0;
			dcg += tempdcg;
			}		
		}
		return dcg;
	}

	double gain(String doc_id, Set<Integer> topic, int query_id, HashMap<Integer, Double> topic_gain) {
		
		double total_gain = 0.0;

		for (Integer intent : topic) {
			if (qrel_dict.get(query_id).get(doc_id).get(intent) > 0) {
				if (!topic_gain.containsKey(intent)) {
					topic_gain.put(intent, 1.0);
					total_gain += topic_gain.get(intent);
				} else {
					total_gain += topic_gain.get(intent);
				}
				double tg = topic_gain.get(intent);
				tg *= 1.0 - this.alpha;
				topic_gain.put(intent, tg);
			}
		}

		return total_gain;
	}

	double discount(int rank) {

		double disc = Math.log(rank - 2) / Math.log(2);
		return disc;
	}

	double i_gain(String doc_id, Set<Integer> topic, int query_id, HashMap<Integer, Double> topic_gain) {

		double total_gain = 0.0;

		for (Integer intent : topic) {
			if (qrel_dict.get(query_id).get(doc_id).get(intent) > 0) {
				if (!topic_gain.containsKey(intent)) {
					topic_gain.put(intent, 1.0);
					total_gain += topic_gain.get(intent);
				} else {
					total_gain += topic_gain.get(intent);
				}
			}
		}
		return total_gain;
	}

	double compute(HashMap<Integer, Set<Integer>> topics, ArrayList<String> doc_ids, int query_id, int depth) {

		double dcg = dcg(doc_ids, topics.get(query_id), query_id, depth);
		double idcg = idcg(topics.get(query_id), query_id, depth);
		
		double a_ndcg = dcg/idcg;
		if(Double.isNaN(a_ndcg) || Double.isInfinite(a_ndcg))
			a_ndcg = 0.0;
		return a_ndcg;
	}
	


	public static void main(String[] args) {
		
		String qrel_path = System.getProperty("user.dir") + "/src/irdm/data/qrels.ndeval.txt";
		String res_path = System.getProperty("user.dir") + "/src/irdm/data/PortfolioScoring4.res";

		HashMap<Integer, ArrayList<String>> result = new HashMap<Integer, ArrayList<String>>();
		

		
		try {
			BufferedReader ifile = new BufferedReader(new FileReader(new File(res_path)));
			String[] split;
			String line;
			
			while((line = ifile.readLine()) != null)
			{
				split = line.split(" ");
				int query_id = Integer.parseInt(split[0]);
				
				String toRemove = "/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/Dataset/clueweb12//";
				String removePost = ".txt";
				String temp = split[1].replace(toRemove, "");
				String docid = temp.replace(removePost, "");
				
				
				if (!result.containsKey(query_id)) {
					ArrayList <String> doc_id = new ArrayList<String>();
					doc_id.add(docid);
					result.put(query_id, doc_id);
				} else {
					result.get(query_id).add(docid);
				}
			}
			ifile.close();
//			for(Map.Entry<Integer, ArrayList<String>> r : result.entrySet()){
//				System.out.println(r.getKey() + " " + r.getValue());
//			}
			
			int [] depth = {1, 5, 10, 20, 30, 40, 50};
			double [] alphas = {0.1, 0.5, 0.9};
			
			ArrayList<Qrel> qrel_list = Utils.loadQrels(qrel_path, QREL.DIVERSITY);
			//HashMap<Integer, Set<Integer>> topic_dict = loadJudgment(qrel_list);
			 
			for(double alpha : alphas){
				
				AlphaNDCG a_ndcg = new AlphaNDCG(alpha);
				HashMap<Integer, Set<Integer>> topic_dict = a_ndcg.loadJudgment(qrel_list);

				for(int k : depth){
					
					double total_andcg = 0.0;
					
					for(Integer query_id : result.keySet()){
						
						double andcg = a_ndcg.compute(topic_dict, result.get(query_id), query_id, k);
						total_andcg += andcg;

					}

						System.out.printf("AlphaNDCG at k=%d alpha=%.1f is %.3f\n",k, alpha, total_andcg / 44);
					
				}
				
			}
			
			
			
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

}
