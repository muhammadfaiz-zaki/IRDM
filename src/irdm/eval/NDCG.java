package irdm.eval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.terrier.applications.batchquerying.TRECQuery;

import irdm.utils.*;
import irdm.utils.Utils.QREL;

public class NDCG {

	private double ndcg;

	private NDCG(int k) {

		System.setProperty("terrier.home",
				"/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/terrier-core-4.1");

		// Topic file path
		String topic_file_path = "/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/Dataset/trec2013-topics.txt";

		// Index path
		String index_path = "/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/terrier-core-4.1/var/index/";

		// Qrel path
		String qrel_path = "/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/Dataset/qrels.adhoc.txt";

		// Load the topics
		TRECQuery trec_topics = new TRECQuery(topic_file_path);
		
		// Load the qrels
		ArrayList<Qrel> qrels = Utils.loadQrels(qrel_path, QREL.ADHOC);

		// Initialize the scorer
		SimpleScoring scorer = new SimpleScoring(index_path, "terrier_clueweb_index");

		ArrayList<Result> res = new ArrayList<Result>();
		ArrayList<Qrel> qrel = new ArrayList<Qrel>();
		Result tempResult = null;
		Qrel tempQrel = null;

		int i = 0;

		while (trec_topics.hasNext()) {
			String query = trec_topics.next();
			HashMap<String, Double> scores = scorer.buildResultSet(i + "", query, k);

			for (Map.Entry<String, Double> entry : scores.entrySet()) {
				//System.out.println(i + "\t" + entry.getKey() + "\t" + entry.getValue());
				tempResult = new Result(entry.getKey(), entry.getValue());
				tempQrel = new Qrel(Integer.toString(i + 201), entry.getKey(),
						Integer.toString(Utils.getRelByID(qrels, i + 201, entry.getKey())));
				if (tempResult != null && tempQrel != null) {
					res.add(tempResult);
					qrel.add(tempQrel);
				}
			}
			i++;
		}
		scorer.closeIndex();

		this.ndcg = compute(res, qrel, k);

	}

	/*
	 * *
	 * 
	 * @param retrieved_list : list of documents, the highest - ranking document
	 * first .
	 * 
	 * @param qrel_list : a collection of labelled document ids from qrel .
	 * 
	 * @param z : cut - off for calculation of NDCG@k
	 * 
	 * @return the NDCG for given data
	 */

	public static double compute(ArrayList<Result> retrieved_list, ArrayList<Qrel> qrel_list, int z) {

		double rel;
		double firstRel = 0.0;
		double sumNDCG = 0.0;

		ArrayList<Double> dcg = new ArrayList<Double>();
		ArrayList<Double> ndcg = new ArrayList<Double>();

		for (int i = 0; i < retrieved_list.size(); i += z) {
			firstRel = qrel_list.get(i).getJudgment();

			if (z == 1) {
				dcg.add(firstRel);
			} else {
				ArrayList<Qrel> temp = new ArrayList<Qrel>(qrel_list.subList(i + 1, i + z));

				int rank = 2;
				rel = 0.0;
				for (Qrel temps : temp) {
					rel += temps.getJudgment() / (Math.log(rank) / Math.log(2));
					rank++;
				}
				dcg.add(firstRel + rel);
			}
		}
		
		
		ArrayList<Double> idcg = computeIDCG(qrel_list, z);

		for (int index = 0; index < dcg.size(); index++) {
			double NDCG = dcg.get(index) / idcg.get(index);
			if (Double.isNaN(NDCG))
				NDCG = 0.0;
			ndcg.add(NDCG);
		}

		for (Double sum : ndcg) {
			sumNDCG += sum;

		}

		double avgNDCG = sumNDCG / 50;
		System.out.println(avgNDCG);

		return avgNDCG;

	}

	static ArrayList<Double> computeIDCG(ArrayList<Qrel> qrel_list, int k) {

		double relIDCG;
		double firstRel = 0.0;

		ArrayList<Double> idcg = new ArrayList<Double>();

		for (int i = 0; i < qrel_list.size(); i += k) {

			ArrayList<Qrel> temp = new ArrayList<Qrel>(qrel_list.subList(i, i + k));
			ArrayList<Integer> sortedRel = new ArrayList<Integer>();

			for (Qrel rel : temp) {
				sortedRel.add(rel.getJudgment());
			}

			Collections.sort(sortedRel);
			Collections.reverse(sortedRel);
			firstRel = sortedRel.get(0).doubleValue();

			if (k == 1) {
				idcg.add(firstRel);
			} else {
				int rank = 2;
				relIDCG = 0.0;
				for (int index = 1; index < sortedRel.size(); index++) {
					relIDCG += sortedRel.get(index).doubleValue() / (Math.log(rank) / Math.log(2));
					rank++;
				}

				idcg.add(firstRel + relIDCG);
			}
		}
		return idcg;
	}

	public String toString() {
		String d = Double.toString(ndcg);
		return d;
	}

	public static void main(String[] args) {
		
//		NDCG test = new NDCG(99);
		ArrayList<Double> answer = new ArrayList<Double>();
		
		for(int i = 1; i<51; i+=10){
			if(i == 1){
				System.out.println(i);
				NDCG test = new NDCG(i);
				answer.add(Double.parseDouble(test.toString()));
				i = i-6;
			}
			else if(i == 5){
				System.out.println(i);
				NDCG test = new NDCG(i);
				answer.add(Double.parseDouble(test.toString()));
				i = 0;
			}
			else{
			System.out.println(i);
			NDCG test = new NDCG(i);
			answer.add(Double.parseDouble(test.toString()));
			}
		}
		
		
		System.out.println("\n\nbm25");
		System.out.println("K \t|\t NDCG@K");
		int g = 1;
		for(Double a : answer){
			if(g==1){
			System.out.printf(g+" \t|\t %.4f\n",a);
			g = g+4;
			}
			else if(g==5){
				System.out.printf(g+" \t|\t %.4f\n",a);
				g = g+5;
			}
			else{
				System.out.printf(g+" \t|\t %.4f\n",a);
				g += 10;
			}
		}
	}
}
