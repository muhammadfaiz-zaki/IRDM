package irdm.eval;

public class Result {
	  String document_id;
	  int document_rank;
	  double score;

	  // Add the constructors.
	  public Result(String doc_id, double score){
		  String [] splitID = doc_id.split(" ");
		  
		  String toRemove = "/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/Dataset/clueweb12//";
		  String removePost = ".txt";
		  String temp = splitID[1].replace(toRemove, "");
		  String docID = temp.replace(removePost, "");
		  
		  this.document_id = docID;
		  this.document_rank = Integer.parseInt(splitID[0])+1;
		  this.score = score;
	  }
	  
	//return the document_id
		public String getDocumentId(){
			return document_id;
		}
		
		//return the document score
		public double getDocumentScore(){
			return score;
		}
		
		//return the document rank
		public int getDocumentRank(){
			return document_rank;
		}
		
		public void setDocumentId(String id){
			this.document_id = id;
		}
		
		public void setDocumentScore(double scr){
			this.score = scr;
		}
		
		public void setDocumentRank(int rank){
			this.document_rank = rank;
		}
		
		public String toString(){
			return document_id + " " + document_rank + " " + score;

		}
	}
