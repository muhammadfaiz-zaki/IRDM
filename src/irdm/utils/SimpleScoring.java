package irdm.utils;


import java.io.IOException;
import java.util.HashMap;

import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;

import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Index;
import org.terrier.structures.Lexicon;
import org.terrier.utility.ApplicationSetup;


public class SimpleScoring {
	/* Terrier Index */
	Index index;
	
	/* Index structures*/
	/* list of terms in the index */
	Lexicon<String> term_lexicon = null;
	/* list of documents in the index */
	DocumentIndex doi = null;
	
	/* Collection statistics */
	long total_tokens;
	long total_documents;
	
	/* Initialize Simple model with index. Use 
	 * @param index_path : initialize index 
	 * @param prefix : language prefix for index 
	 * with location of index created using bash script.
	 */
	public SimpleScoring(String index_path, String prefix) {
		
		// Load the index and collection stats
		try {
			index = Index.createIndex(index_path, prefix);
			
			System.out.println("Loaded index from path "+index_path+" "+index.toString());
			
			total_tokens = index.getCollectionStatistics().getNumberOfTokens();
			total_documents = index.getCollectionStatistics().getNumberOfDocuments();
			System.out.println("Number of terms and documents in index "+total_tokens+" "+total_documents);
			
	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public HashMap <String, Double> buildResultSet(String id, String query, int k){
		// Just find documents and their posting list for the query. 
		
		
		// Create a search request object.
		Manager manager = new Manager(this.index);
		SearchRequest srq = manager.newSearchRequest(id, query);
		
		// Get the results using tfidf
		srq.addMatchingModel("Matching", "BM25");
		manager.runPreProcessing(srq);
		manager.runMatching(srq);
		manager.runPostProcessing(srq);
		manager.runPostFilters(srq);
		
		ResultSet set = srq.getResultSet();
		//int doc_ids [] = set.getDocids();
	
		double doc_scores [] = set.getScores();
		
		
		final String metaIndexDocumentKey = ApplicationSetup.getProperty(
				"trec.querying.outputformat.docno.meta.key", "filename");
		String doc_names [] = Utils.getDocnos(metaIndexDocumentKey, set, index);
		
		
		HashMap <String, Double> scores = new HashMap<String, Double>();
		HashMap <String, Double> sortedScores = new HashMap<String, Double>();
		
		
		for (int i = 0 ; i < doc_scores.length;i++){
			//System.out.println(doc_ids[i]+" "+doc_scores[i]);
			//System.out.println(i + " " + doc_names[i]+" "+doc_scores[i]);
			scores.put(i + " " + doc_names[i], doc_scores[i]);
			
			if (i==k-1)
				break;
		}
			
		sortedScores = Utils.sortByComparator(scores);
		 
		return sortedScores;
		
	}
	
	public void closeIndex() {
		try {
			index.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}
