package irdm.diversity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.terrier.applications.batchquerying.TRECQuery;
import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Index;
import org.terrier.structures.Lexicon;
import org.terrier.utility.ApplicationSetup;

import irdm.utils.Utils;
import java.io.FileWriter;

public class MMRScoring {

	/* Terrier Index */
	Index index;

	/* Index structures */
	/* list of terms in the index */
	Lexicon<String> term_lexicon = null;
	/* list of documents in the index */
	DocumentIndex doi = null;

	/* Collection statistics */
	long total_tokens;
	long total_documents;

	ArrayList<HashMap<Integer, Double>> tfidfPerQuery;

	/*
	 * Initialize MMR model with index. Use
	 * 
	 * @param index_path : initialize index
	 * 
	 * @param prefix : language prefix for index (default = 'en') with location
	 * of index created using bash script.
	 */
	public MMRScoring(String index_path, String prefix) {

		// Load the index and collection stats
		try {
			index = Index.createIndex(index_path, prefix);

			System.out.println("Loaded index from path " + index_path + " " + index.toString());

			this.total_tokens = index.getCollectionStatistics().getNumberOfTokens();
			this.total_documents = index.getCollectionStatistics().getNumberOfDocuments();
			System.out.println("Number of terms and documents in index " + total_tokens + " " + total_documents);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public double getSimilarity(int document_id1, int document_id2,
			ArrayList<HashMap<Integer, Double>> tfidfCollection) {

		float dotProduct = 0;
		float squareValDoc1 = 0;
		float squareValDoc2 = 0;
		double magDoc1 = 0;
		double magDoc2 = 0;
		double cosineSim = 0;

		HashMap<Integer, Double> doc1 = tfidfCollection.get(document_id1);
		for (Map.Entry<Integer, Double> term : doc1.entrySet()) {
			if (tfidfCollection.get(document_id2).containsKey(term.getKey())) {
				dotProduct += term.getValue() * tfidfCollection.get(document_id2).get(term.getKey());
				squareValDoc1 += Math.pow(term.getValue(), 2);
				squareValDoc2 += Math.pow(tfidfCollection.get(document_id2).get(term.getKey()), 2);
			}
		}

		magDoc1 = Math.sqrt(squareValDoc1);
		magDoc2 = Math.sqrt(squareValDoc2);

		cosineSim = dotProduct / (magDoc1 * magDoc2);
		return cosineSim;
	}

	public double scoreDocument(double sim, double lambda) {

		double score = (1 - lambda) * sim;
		return score;
	}

	public HashMap<String, Double> buildResultSetMMR(String key, HashMap<String, Double> mmrCollection,
			HashMap<String, Double> scores, double f2, double lambda) {

		double mmr = 0;

		mmr = lambda * scores.get(key) - f2;
		mmrCollection.put(key, mmr);

		return mmrCollection;

	}

	public HashMap<String, Double> buildResultSet(String id, String query, int k) {
		// Just find documents and their posting list for the query.

		// Create a search request object.
		Manager manager = new Manager(this.index);
		SearchRequest srq = manager.newSearchRequest(id, query);

		// Get the results using tfidf
		srq.addMatchingModel("Matching", "BM25");
		manager.runPreProcessing(srq);
		manager.runMatching(srq);
		manager.runPostProcessing(srq);
		manager.runPostFilters(srq);

		ResultSet set = srq.getResultSet();
		int doc_ids[] = set.getDocids();
		double doc_scores[] = set.getScores();

		final String metaIndexDocumentKey = ApplicationSetup.getProperty("trec.querying.outputformat.docno.meta.key",
				"filename");
		String doc_names[] = Utils.getDocnos(metaIndexDocumentKey, set, index);

		HashMap<String, Double> scores = new HashMap<String, Double>();
		HashMap<String, Double> sortedScores = new HashMap<String, Double>();
		HashMap<Integer, Double> termTF = new HashMap<Integer, Double>();

		tfidfPerQuery = new ArrayList<HashMap<Integer, Double>>();
		for (int i = 0; i < doc_scores.length; i++) {
			// System.out.println(doc_ids[i]+" "+doc_scores[i]);
			System.out.println(i + " " + doc_names[i] + " " + doc_scores[i]);
			scores.put(doc_names[i], doc_scores[i]);
			// System.out.println("ID: " + doc_ids[i] + " " + doc_names[i]);
			// normalized TF in doc
			termTF = Utils.docTF(doc_ids[i], this.index);

			tfidfPerQuery.add(termTF);
			if (i == k - 1) {
				break;
			}
		}

		sortedScores = Utils.sortByComparator(scores);

		return sortedScores;
	}

	public void closeIndex() {
		try {
			index.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		System.setProperty("terrier.home",
				"/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/terrier-core-4.1");

		// Topic file path
		String topic_file_path = "/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/Dataset/trec2013-topics.txt";

		// Index path
		String index_path = "/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/terrier-core-4.1/var/index/";

		// Load the topics
		TRECQuery trec_topics = new TRECQuery(topic_file_path);


		// Initialize the scorer
		MMRScoring scorer = new MMRScoring(index_path, "terrier_clueweb_index");

		int i = 0;
		HashMap<String, Double> mmr = new HashMap<String, Double>();
		HashMap<String, Double> sortedMMR = new HashMap<String, Double>();

		try {
			FileWriter writer = new FileWriter(System.getProperty("user.dir") + "/src/irdm/data/MMRscoring0.25.res");
			while (trec_topics.hasNext()) {

				mmr = new HashMap<String, Double>();
				List<String> s_Space = new ArrayList<String>();
				List<Double> simSpace = new ArrayList<Double>();
				double lambda = 0.25;

				String query = trec_topics.next();
				HashMap<String, Double> scores = scorer.buildResultSet(i + "", query, 100);

				List<String> docNames = new ArrayList<String>(scores.keySet());
				double sim = 0;
				double maxSim = 0;

				if (docNames.isEmpty())
					continue;
				s_Space.add(docNames.get(0));
				for (int j = 1; j < docNames.size(); j++) {
					for (int l = 0 + s_Space.size(); l < docNames.size(); l++) {
						for (int k = 0; k < s_Space.size(); k++) {
							// System.out.println("k " + k + " " + "l " + l);
							sim = scorer.getSimilarity(k, l, scorer.tfidfPerQuery);
							simSpace.add(sim);
						}

						maxSim = Collections.max(simSpace);
						double f2 = scorer.scoreDocument(maxSim, lambda);
						mmr = scorer.buildResultSetMMR(docNames.get(l), mmr, scores, f2, lambda);
					}

					Entry<String, Double> maxEntry = null;
					for (Entry<String, Double> entry : mmr.entrySet()) {
						if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
							maxEntry = entry;
						}
					}

					s_Space.add(maxEntry.getKey());
				}
				sortedMMR = Utils.sortByComparator(mmr);
				for (Map.Entry<String, Double> entry : sortedMMR.entrySet()) {
					writer.write(i + 201 + " " + entry.getKey() + " " + entry.getValue() + "\n");
				}

				i++;
			}
			scorer.closeIndex();
			writer.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

}
