package irdm.diversity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.FileWriter;

import org.terrier.applications.batchquerying.TRECQuery;
import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Index;
import org.terrier.structures.Lexicon;
import org.terrier.utility.ApplicationSetup;

import irdm.utils.Utils;

public class PortfolioScoring {

	/* Terrier Index */
	Index index;

	/* Index structures */
	/* list of terms in the index */
	Lexicon<String> term_lexicon = null;
	/* list of documents in the index */
	DocumentIndex doi = null;

	/* Collection statistics */
	long total_tokens;
	long total_documents;

	ArrayList<HashMap<Integer, Double>> tfidfPerQuery;

	public PortfolioScoring(String index_path, String prefix) {

		// Load the index and collection stats
		try {
			index = Index.createIndex(index_path, prefix);

			System.out.println("Loaded index from path " + index_path + " " + index.toString());

			this.total_tokens = index.getCollectionStatistics().getNumberOfTokens();
			this.total_documents = index.getCollectionStatistics().getNumberOfDocuments();
			System.out.println("Number of terms and documents in index " + total_tokens + " " + total_documents);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/*
	 * * Computes correlation between term vectors of 2 documents .
	 */
	public double getCorrelation(int document_id1, int document_id2,
			ArrayList<HashMap<Integer, Double>> tfidfCollection) {
		double dotProduct = 0;
		double sumOfX = 0;
		double sumOfY = 0;
		double squareValDoc1 = 0;
		double squareValDoc2 = 0;
		double magDoc1 = 0;
		double magDoc2 = 0;
		double pearsonR = 0;

		HashMap<Integer, Double> doc1 = tfidfCollection.get(document_id1);
		double n = doc1.size();
		for (Map.Entry<Integer, Double> term : doc1.entrySet()) {
			if (tfidfCollection.get(document_id2).containsKey(term.getKey())) {
				dotProduct += term.getValue() * tfidfCollection.get(document_id2).get(term.getKey());
				sumOfX += term.getValue();
				sumOfY += tfidfCollection.get(document_id2).get(term.getKey());
				squareValDoc1 += Math.pow(term.getValue(), 2);
				squareValDoc2 += Math.pow(tfidfCollection.get(document_id2).get(term.getKey()), 2);
			}
		}

		magDoc1 = Math.sqrt((squareValDoc1 - (Math.pow(sumOfX, 2) / n)));
		magDoc2 = Math.sqrt((squareValDoc2 - (Math.pow(sumOfY, 2) / n)));

		pearsonR = (dotProduct - (sumOfX * sumOfY / n)) / (magDoc1 * magDoc2);

		return pearsonR;
	}

	/* * Computes score of document * */
	public double scoreDocument(int document_id, double b, double sumRedundancy, HashMap<String, Double> scores,
			List<String> docname) {
		double docScore = scores.get(docname.get(document_id));
		double weight = 1 / Math.pow(2, document_id - 1);

		double mvaScore = docScore - (b * weight) - (2 * b) * sumRedundancy;
		return mvaScore;
	}

	public HashMap<String, Double> buildResultSet(String id, String query, int k) {
		// Just find documents and their posting list for the query.

		// Create a search request object.
		Manager manager = new Manager(this.index);
		SearchRequest srq = manager.newSearchRequest(id, query);

		// Get the results using tfidf
		srq.addMatchingModel("Matching", "BM25");
		manager.runPreProcessing(srq);
		manager.runMatching(srq);
		manager.runPostProcessing(srq);
		manager.runPostFilters(srq);

		ResultSet set = srq.getResultSet();
		int doc_ids[] = set.getDocids();

		double doc_scores[] = set.getScores();

		final String metaIndexDocumentKey = ApplicationSetup.getProperty("trec.querying.outputformat.docno.meta.key",
				"filename");
		String doc_names[] = Utils.getDocnos(metaIndexDocumentKey, set, index);

		HashMap<String, Double> scores = new HashMap<String, Double>();
		HashMap<String, Double> sortedScores = new HashMap<String, Double>();
		HashMap<Integer, Double> termTF = new HashMap<Integer, Double>();

		tfidfPerQuery = new ArrayList<HashMap<Integer, Double>>();
		for (int i = 0; i < doc_scores.length; i++) {

			scores.put(doc_names[i], doc_scores[i]);

			termTF = Utils.docTF(doc_ids[i], this.index);

			tfidfPerQuery.add(termTF);
			if (i == k - 1) {
				break;
			}
		}

		sortedScores = Utils.sortByComparator(scores);

		return sortedScores;
	}

	public void closeIndex() {
		try {
			index.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		System.setProperty("terrier.home",
				"/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/terrier-core-4.1");

		// Topic file path
		String topic_file_path = "/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/Dataset/trec2013-topics.txt";

		// Index path
		String index_path = "/Users/mfaizmzaki/Desktop/UCL_courses/Term_2/Information_Retrieval/Assignment/terrier-core-4.1/var/index/";

		// Load the topics
		TRECQuery trec_topics = new TRECQuery(topic_file_path);

		// Initialize the scorer
		PortfolioScoring scorer = new PortfolioScoring(index_path, "terrier_clueweb_index");

		int i = 0;
		double b = -4.0;
		
		HashMap<String, Double> sortedMVA = new HashMap<String, Double>();

		try {
			FileWriter writer = new FileWriter(System.getProperty("user.dir") + "/src/irdm/data/PortfolioScoring-4.res");

			while (trec_topics.hasNext()) {

				String query = trec_topics.next();
				HashMap<String, Double> scores = scorer.buildResultSet(i + "", query, 100);

				List<String> docNames = new ArrayList<String>(scores.keySet());

				if (docNames.isEmpty())
					continue;
				for (int j = 0; j < docNames.size(); j++) {
					double sumRedundancy = 0;
					for (int k = 0; k < docNames.size(); k++) {
						if (k == j)
							continue;
						double weight = 1 / Math.pow(2, k - 1);
						double r = scorer.getCorrelation(j, k, scorer.tfidfPerQuery);
						sumRedundancy += weight * r;
					}
					double score = scorer.scoreDocument(j, b, sumRedundancy, scores, docNames);
					scores.put(docNames.get(j), score);

				}

				sortedMVA = Utils.sortByComparator(scores);
				for (Map.Entry<String, Double> entry : sortedMVA.entrySet()) {
					writer.write(i + 201 + " " + entry.getKey() + " " + entry.getValue() + "\n");
				}

				i++;
			}
			scorer.closeIndex();
			writer.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

}
